from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QApplication
import sys


def run():
    app = QApplication(sys.argv)
    engine = QQmlApplicationEngine()
    engine.load('views/main.qml')

    return -1 if not engine.rootObjects() else app.exec_()


if __name__ == "__main__":
    sys.exit(run())